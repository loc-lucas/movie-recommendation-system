import { Layout, Menu, Avatar } from 'antd';
import {
  HeartFilled,
  FireFilled,
  SearchOutlined,
  UserOutlined
} from '@ant-design/icons';
import React, { Dispatch, SetStateAction, useEffect, useRef, useState } from 'react';
import styles from '../../../styles/Home.module.css'

import Movies from '../../Movies';
import { Input, Space } from 'antd';
import firebase from '../../../firebase/clientApp';
import { Movie } from '../../../models/movie';
import { useRouter } from 'next/router';

const { Search } = Input;

const { Content, Sider } = Layout;


interface SidebarProps {
  children: any;
  correct: ()=>void
}

//////////////////////////////////////////////////

const db = firebase.firestore();
const userCollection = db.collection("user");
const movieCollection = db.collection("movie");

const user_id = "1000";

async function getMovieInfo(id: string) {
  const movieInfo = await movieCollection.doc(id).get();
  const movieData = movieInfo.data();

  const thumbnail = movieData?.thumbnail
  const title = movieData?.title
  const genre = movieData?.genre
  const stars = movieData?.stars
  const director = movieData?.director
  const storyLine = movieData?.storyline

  return [id, thumbnail, title, genre, stars, director, storyLine]
}


async function getMovieList(user_id: string, collection: string, movie_list: Movie[]) {
  const movieList = await userCollection.doc(user_id).get();
  let movieIdList = []
  if (collection == "favorite") {
    movieIdList = movieList.data()?.favorite;
  } else {
    movieIdList = movieList.data()?.recommend;
  }

  for (let key in movieIdList) {
    const [id, thumbnail, title, genre, stars, director, storyLine] = await getMovieInfo(key)
    const obj = {
      movies_id: id,
      thumbnail: thumbnail,
      title: title,
      genre: genre,
      stars: stars,
      director: director,
      storyLine: storyLine
    }

    movie_list.push(obj)
  }
}

async function getSearchList(value: string, searchList: Movie[]) {
  let keyword = value.charAt(0).toUpperCase() + value.slice(1);
  const result = await movieCollection.orderBy('title').startAt(keyword).endAt(keyword + "\uf8ff").get();
  
  if (value == "") {
    return searchList;
  }

  result.forEach(doc => {
    const movieData = doc.data();
    const titleInDB = movieData.title;
    searchList.push({
      movies_id: doc.id,
      thumbnail: movieData.thumbnail,
      title: titleInDB,
      genre: movieData.genre,
      stars: movieData.stars,
      director: movieData.director,
      storyLine: movieData.storyline
    })
    // if (titleInDB.includes(value) || titleInDB.toLowerCase().includes(value)) {}
  })

  return searchList;
}

// getRecommendList(user_id, recommend);

/////////////////////////////////////////////////////// SIDEBAR

const Sidebar = ({ children, correct }: SidebarProps) => {
  let firstMounted = useRef(false);
  let search: Movie[] = []
  let recommend: Movie[] = []
  let favorite: Movie[] = []
  const [selectedMenuItem, setSelectedMenuItem] = useState('1');
  const [recommendList, setRecommendList] = useState<Movie[]>([]);
  const [favouriteList, setFavouriteList] = useState<Movie[]>([]);
  const [searchList, setSearchList] = useState<Movie[]>([]);


  const onSearch = async (value: string) => {
    search = await getSearchList(value, search);
    setSearchList([...search])
  };

  //////////////////////////////////////////////////

  function remove(key: number) {
    let newLst = [...favouriteList]
    newLst.splice(key, 1)
    setFavouriteList([...newLst])
  }

  function logOut(){
    useRouter().push('/login')
  }

  function switchContent(key: any) {
    switch (key) {
      case '1':
        return <Movies listMovies={recommendList} mod={0} remove={remove}/>
      case '2':
        return <Movies listMovies={favouriteList} mod={1} remove={remove}/>
      case '3':
        return (
          <>
            <Search
              placeholder="input search text"
              allowClear
              enterButton="Search"
              size="large"
              onSearch={onSearch}
              className='mt-4'
            />

            <Movies listMovies={searchList} mod={2} remove={remove}/>
          </>)
    }
  }

  useEffect(() => {
    (async () => {
    firstMounted.current = true;
    switch (selectedMenuItem){
      case '1': 
        recommend = []
        await getMovieList(user_id, "recommend", recommend);
        setRecommendList([...recommend])
        break
      case '2':
        favorite = []
        await getMovieList(user_id, "favorite", favorite);
        setFavouriteList([...favorite])
        break
      case '3':
        break
    }
    })()
  }, [selectedMenuItem]);

  return (
    <Layout style={{ minHeight: '100vh' }}>
      <Sider trigger={null}>
        <div className={styles.logo}>
          <img src="popcorn.png" width="70" height="70" />
          <p className='mt-3' style={{ color: 'white', fontSize: '0.85rem', fontWeight: 'bolder' }}>NETFLIX & CHILL</p>
        </div>
        <Menu theme="dark" defaultSelectedKeys={['1']} mode="inline" selectedKeys={[selectedMenuItem]}
          onClick={(e) => { setSelectedMenuItem(e.key) }}>
          <Menu.Item key={'1'} icon={<FireFilled />}>
            Recommended
          </Menu.Item>
          <Menu.Item key={'2'} icon={<HeartFilled />}>
            Favorite
          </Menu.Item>
          <Menu.Item key={'3'} icon={<SearchOutlined />}>
            Search
          </Menu.Item>
        </Menu>
        <div className='mt-4'>
          <div className='flex align-center justify-center'>
            <Avatar size={64} icon={<UserOutlined />} />
          </div>
          <div className='flex align-center justify-center mt-4' style={{ color: 'white' }}>John Doe</div>
          <div onClick={logOut} className='flex align-center justify-center mt-4' style={{ color: 'gray' }}>Log out</div>
        </div>
      </Sider>
      <Layout className="site-layout">
        <Content style={{ margin: '0 16px' }}>
          {switchContent(selectedMenuItem)}
        </Content>
      </Layout>
    </Layout>
  );
}

export default Sidebar