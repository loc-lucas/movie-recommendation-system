import { Row, Col, Modal, Button, Rate } from "antd"
import React, { useEffect, useState } from "react"
import { Movie } from "../../models/movie"
import firebase from '../../firebase/clientApp';
import { setgroups } from "process";

const db = firebase.firestore();
const user_id = "1000"

type MoviesListProp = {
  listMovies: any[];
  mod: number; // 0 is recommended, 1 is favorite
  remove: (i: number)=>void
}

const Movies = ({
  listMovies,
  mod,
  remove
}: MoviesListProp) => {
  const [modalShow, setModalShow] = useState(false)
  const [movie, setMovie] = useState(listMovies[0])
  const [rating, setRating] = useState(0)
  const [pos, setPos] = useState(0)

  async function RateMovie(){
    const movieList = await db.collection("user").doc(user_id).get();
    let favoriteList = movieList.data()?.favorite;
    favoriteList[movie.movies_id] = rating;
    
    db.collection("user").doc(user_id).update({
      favorite: favoriteList
    })
    setModalShow(false)

    if (Object.keys(favoriteList).length >= 5) {
      await fetch(`http://localhost:8000/users/${user_id}`)
    }
    return null
  }

  async function RemoveMovie(){
    // Remove movie trong favorite list tren firestore
    const movieList = await db.collection("user").doc(user_id).get();
    let favoriteList = movieList.data()?.favorite;
    delete favoriteList[movie.movies_id];

    db.collection("user").doc(user_id).update({
      favorite: favoriteList
    })
    remove(pos)
    setModalShow(false)
    return null
  }

  function ModalButton({ mod }: { mod: number }) {
    switch (mod) {
      case 0:
        return (<></>)
      case 1:
        return (
          <>
          <div className='flex place-items-center flex-col'>
            <Rate allowHalf defaultValue={rating} onChange={setRating}/>
            <Button key="submit" type="primary" className='mt-4' onClick={RateMovie}>
              Rate
            </Button>,
          </div>
          <div className='flex place-items-center flex-col'>
          <Button key="submit" type="primary" style={{backgroundColor:'red'}} onClick={RemoveMovie}>
            Remove
          </Button>,
          </div>
          </>
        )
      case 2:
        return (
          <div className='flex place-items-center flex-col'>
            <Rate allowHalf defaultValue={rating} onChange={setRating}/>
            <Button key="submit" type="primary" className='mt-4' onClick={RateMovie}>
              Rate
            </Button>,
          </div>
        )
    }
    return <></>
  }

  return (
    <>
      <div style={{ height: '5vh' }} />
      <Row gutter={[16, 24]} >
        {
          listMovies?.map((movie: any, i: number) =>
            <Col className="gutter-row relative mt-4 mb-4" style={{height:'70vh'}} span={6} key={i}>
              <div style={{height:'96%'}}>
                <img src={movie.thumbnail} style={{ width: '100%'}} onClick={(e) => {
                  setMovie(listMovies[i]);
                  setPos(i);
                  setModalShow(true)
                }} />
              </div>
              <div className='flex justify-center' style={{ fontSize: '15px', fontWeight: 'bold' }}>{movie.title}</div>
            </Col>)
        }
      </Row>
      <Modal
        visible={modalShow}
        title={<div style={{ fontSize: '1.5rem', fontWeight: 'bold' }} className='flex justify-center'>{movie?.title}</div>}
        onCancel={() => { setModalShow(false) }}
        width='70%'
        style={{top:'3rem'}}
        footer={[
          <ModalButton mod={mod} />
        ]}
      >
        <div className='flex relative overflow-y-clipped'>
          <img src={movie?.thumbnail} style={{ width: '30%' }} className='mr-2' />
          <div style={{width: '70%'}}>
            <Row gutter={16} style={{ maxHeight: '7rem' }}>
              {
                movie?.genre.map((genre: string, i: number) =>
                <Genre genre={genre} />)
              }
            </Row>
            <div className='mt-4'>
              <span className='ml-2' style={{ fontWeight: 'bold' }}>Director: </span>
              <span>{movie?.director}</span>
            </div>
            <div className='mt-4'>
              <span className='ml-2' style={{ fontWeight: 'bold' }}>Stars: </span>
              <span>{(() => {
                let stars = ''
                movie?.stars.forEach((item: string, index: number) => {
                  if (index === movie.stars.length - 1) {
                    stars = stars + item
                  }
                  else stars = stars + item + ', '
                })
                return stars
              })()}</span>
            </div>
            <div className='ml-2 mt-4'>
              <span style={{ fontWeight: 'bold' }}>Story Line: </span>
              <span>{movie?.storyLine}</span>
            </div>
          </div>
        </div>
      </Modal>
    </>
  )
}

export default Movies

function Genre({ genre }: { genre: string }) {
  return (
    <div className='border-black border-1 rounded-md border-solid p-2 flex items-center ml-4' style={{ fontSize: '0.8rem', height: '2.5rem', backgroundColor: '#d6d6d6' }}>{genre}</div>
  )
}