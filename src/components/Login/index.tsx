import styles from '../../styles/Home.module.css'
import { Form, Input, Button, Checkbox, AutoComplete } from 'antd';

export default function Login({
  correct
}: {correct:()=>void}) {

  const onFinish = (values: any) => {
    if (values.username==='username', values.password==='123456')
    correct()
  };

  const onFinishFailed = (errorInfo: any) => {
  };

  return (
    <div className={styles.center}>
    <img src="popcorn.png" width="200"></img>
    <h6 className={styles.title}>NETFLIX AND CHILL</h6>
    <p/>
    <Form
      name="basic"
      labelCol={{
        span: 8,
      }}
      wrapperCol={{
        span: 16,
      }}
      initialValues={{
        remember: true,
      }}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      autoComplete="off"
    >
      <Form.Item
        label="Username"
        name="username"
        rules={[
          {
            required: true,
            message: 'Please input your username!',
          },
        ]}
      >
        <Input />
      </Form.Item>

      <Form.Item
        label="Password"
        name="password"
        rules={[
          {
            required: true,
            message: 'Please input your password!',
          },
        ]}
      >
        <Input.Password />
      </Form.Item>

      <Form.Item
        name="remember"
        valuePropName="checked"
        wrapperCol={{
          offset: 8,
          span: 16,
        }}
      >
        <Checkbox>Remember me</Checkbox>
      </Form.Item>

      <Form.Item
        wrapperCol={{
          offset: 8,
          span: 16,
        }}
      >
        <Button type="primary" htmlType="submit">
          Login
        </Button>
      </Form.Item>
    </Form>
    </div>
  )
}