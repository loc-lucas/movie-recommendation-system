export interface Movie {
    movies_id: string,
    thumbnail: string,
    title: string,
    genre: Array<string>,
    stars: Array<string>,
    director: Array<string>,
    storyLine: string
}