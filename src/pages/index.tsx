import type { NextPage } from 'next'
import Head from 'next/head'
import Image from 'next/image'
import React, { Component, useState } from 'react';
import Sidebar from '../components/Layout/Sider';

import Login from '../components/Login';

const Home: NextPage = () => {
  const [login, setLogin] = useState(false)
  function correct(){
    setLogin(true)
  }
  return (<>{
     login?
      (<>
        <Sidebar correct={correct}>
          <Component/>
        </Sidebar>
      </>):(<Login correct={correct}></Login>)
  }</>
  )
}

export default Home
