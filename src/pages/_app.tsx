import React from "react";
import type { AppProps /*, AppContext */ } from "next/app";
import 'tailwindcss/tailwind.css'

import "../styles/App.css";
import Sidebar from "../components/Layout/Sider";

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <>
        <Component {...pageProps} />
    </>
  );
}

export default MyApp;
